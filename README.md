# Gedimagination Application Web


## Description

Ceci est le repository contant la partie application web du projet Gedimagination.
La documentation se trouve ici : https://gitlab.com/gedima-gination/gedima-gination-doc
l'application android se trouve ici : https://gitlab.com/gedima-gination/gedima-gination-mobile

## Contexte
La société NEGOMAT vient tout juste d’entrer dans le réseau d’adhérents de GEDIMAT. Pour
fêter cet événement, NEGOMAT a décidé d’organiser un jeu concours nommé
« GEDIMA’GINATION ».
PRINCIPE DU JEU : 
- Le jeu est ouvert aux clients de NEGOMAT. Pour participer, il suffit de s’inscrire et poster une photo d’une réalisation faite à partir de matériaux achetés chez NEGOMAT.
- Dans un deuxième temps, les clients de passage dans le magasin voteront pour leurs 3 réalisations préférées. Les photos des réalisations seront consultables sur un écran TV. Les clients utiliseront une borne tablette installée dans le magasin pour saisir leurs votes. A la fin du concours, seront désignés les 3 gagnants. Ceux-ci seront récompensés par des bons d’achat

## Fonctionnement
L'application web permet a un visiteur de consulter le classement si la période de vote est fini 
et que le gérant la publié sinon il peut s'inscrire afin de devenir un participant.
Le participant peut publier une participation pour cela il suffit de s'aisir les informations du 
formulaire, il peut consulter ça participation sur sont compte.





from datetime import date

from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render, redirect

from Gedimagination.forms.dataForm import DataForm
from Gedimagination.lib.generateClassement import generer_classement, make_classement_publique
from Gedimagination.models import Data, Participation
from Gedimagination.lib.generateClassement import get_classement

"""
Gestion de la page admin
"""
@user_passes_test(lambda u: u.is_superuser)
def admin(request):
    #Variables
    user = request.user
    data = Data.objects.first()
    data_form = DataForm()
    date_now = date.today().strftime("%Y-%m-%d")

    # Vérification de présence d'information sur le concours
    if data is None:
        data = Data(debut_participation=date_now, fin_participation=date_now, debut_vote=date_now, fin_vote=date_now, classement_generer=0, classement_publique=0)
        data.save()
        data = Data.objects.first()

    fin_vote = data.fin_vote.strftime("%Y-%m-%d")
    debut_participation = data.debut_participation.strftime("%Y-%m-%d")

    if request.method == 'POST':
        data_form = DataForm(request.POST, request.FILES)
        #Sauvegarde des donées pour la table data
        if data_form.is_valid():
            data.debut_participation = request.POST['debut_participation']
            data.fin_participation = request.POST['fin_participation']
            data.debut_vote = request.POST['debut_vote']
            data.fin_vote = request.POST['fin_vote']
            data.reglement = request.POST['reglement']
            data.save()

        elif request.POST['manager_classement'] == 'generate':
            generer_classement()
        elif request.POST['manager_classement'] == 'publish' and data.classement_generer == 1:
            make_classement_publique()

    data = Data.objects.first()
    participations = Participation.objects.filter(place_classement__range=(1,3)).order_by('place_classement')
    classement = get_classement(participations)

    #Ajout des donées depuis la base dans le formulaire
    form_data = {'debut_participation':data.debut_participation,
                 'fin_participation':data.fin_participation,
                 'debut_vote':data.debut_vote,
                 'fin_vote':data.fin_vote,
                 'reglement':'Test de Julien'
                 }
    data_form = DataForm(form_data, initial=form_data)
    data_form.has_changed()

    print(debut_participation)
    if(debut_participation < date_now):
        data_form.fields['debut_participation'].widget.attrs.update({'disabled': ''})
        data_form.fields['fin_participation'].widget.attrs.update({'disabled': ''})
        data_form.fields['debut_vote'].widget.attrs.update({'disabled': ''})
        data_form.fields['fin_vote'].widget.attrs.update({'disabled': ''})

    return render(request, 'admin.html', {'dataForm': data_form, 'data': data, 'date_now':date_now, 'fin_vote': fin_vote, 'classement': classement})


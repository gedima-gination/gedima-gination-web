from django.shortcuts import render
from django.template import RequestContext

"""
Gestions des erreurs 418
"""
def handler418(request, *args, **argv):
    response = render(request,'error/418.html', {})
    response.status_code = 418
    return response

"""
Gestions des erreurs 400
"""
def handler400(request, *args, **argv):
    response = render(request,'error/400.html', {})
    response.status_code = 404
    return response

"""
Gestions des erreurs 401
"""
def handler401(request, *args, **argv):
    response = render(request,'error/401.html', {})
    response.status_code = 404
    return response

"""
Gestions des erreurs 403
"""
def handler403(request, *args, **argv):
    response = render(request,'error/403.html', {})
    response.status_code = 404
    return response

"""
Gestions des erreurs 404
"""
def handler404(request, *args, **argv):
    response = render(request,'error/404.html', {})
    response.status_code = 404
    return response

"""
Gestions des erreurs 500
"""
def handler500(request, *args, **argv):
    response = render(request, 'error/500.html', {})
    response.status_code = 500
    return response

from django.shortcuts import render
from django.core.mail import send_mail

from Gedimagination.forms.contactForm import ContactForm

"""
Gestion de la page contact
"""
def contact(request):
    contact_form = ContactForm()

    if request.method == 'POST':
        contact = ContactForm(request.POST, request.FILES)
        if contact.is_valid():
            send_mail(
                'Support Gedima\'gination ' + request.POST['email'] + " " + request.POST['full_name'],
                request.POST['message'],
                'contact.gedimagination@gmail.com',
                ['contact.gedimagination@gmail.com'],
                fail_silently=True,
            )


    return render(request, 'contact.html',{'form': contact_form})
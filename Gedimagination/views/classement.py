from datetime import date


from django.shortcuts import render

from Gedimagination.lib.generateClassement import get_classement
from Gedimagination.models import Data, Participation

"""
Gestion de la page classement
"""
def classement(request):
    #Variables
    date_now = date.today().strftime("%Y-%m-%d")
    data = Data.objects.first()

    # Vérification de présence d'information sur le concours
    if data is None:
        return render(request, 'error/dataError.html')
    if data.fin_vote is None:
        return render(request, 'error/dataError.html')

    fin_vote = data.fin_vote.strftime("%Y-%m-%d")
    participations = Participation.objects.filter(place_classement__range=(1,3)).order_by('place_classement')
    classement = get_classement(participations)

    return render(request, 'classement.html', {'data': data, 'date_now':date_now, 'fin_vote': fin_vote, 'classement': classement})



import datetime

from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.shortcuts import render, redirect
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.conf import settings

from Gedimagination.forms.customUserCreationForm import CustomUserCreationForm
from Gedimagination.lib.generateActivationCode import generate_code
from Gedimagination.lib.tokenGenerator import account_activation_token
from Gedimagination.models import AuthUserGroups, AuthGroup, Consentement, Action, AuthUser, Activation

"""
Gestion de la page d'inscription
"""
def register(request):
    if request.method == "POST":
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            if settings.SEND_ACTIVATION_EMAIL is True:
                user = form.save(commit=False)
                user.is_active = False
                user.save()
                auth = AuthUser.objects.filter(id=user.id).first()
                action = Action.objects.filter(id_action=1).first()
                Consentement(id_user=auth, id_action= action, date_consentement=datetime.datetime.now()).save()
                current_site = get_current_site(request)
                code = generate_code()
                Activation(id_user=auth,code=code,date_code=datetime.datetime.now()).save()
                mail_subject = 'Activation link has been sent to your email id'
                message = render_to_string('email/active_email.html', {
                    'user': user,
                    'code': code,
                    'domain': current_site.domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'token': account_activation_token.make_token(user),
                })
                to_email = form.cleaned_data.get('email')
                email = EmailMessage(
                    mail_subject, message, to=[to_email]
                )
                email.send()
                return render(request, 'registration/inactive_account.html')
            else:
                user = form.save(commit=False)
                user.is_active = True
                user.save()
                AuthUserGroups(user_id=user.id, group_id=AuthGroup.objects.filter(name='user').first().id).save()
                auth = AuthUser.objects.filter(id=user.id).first()
                action = Action.objects.filter(id_action=1).first()
                Consentement(id_user=auth, id_action= action, date_consentement=datetime.datetime.now()).save()
                return redirect('login')
    else:

        form = CustomUserCreationForm()

    return render(request, 'registration/register.html', {'form': form})
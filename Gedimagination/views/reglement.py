from django.shortcuts import render

from Gedimagination.models import Data

"""
Gestion de la page reglement
"""
def reglement(request):
    #Variables
    data = Data.objects.first()

    # Vérification de présence d'information sur le concours
    if data is None:
        return render(request, 'error/dataError.html')

    return render(request, 'reglement.html', {'data': data})

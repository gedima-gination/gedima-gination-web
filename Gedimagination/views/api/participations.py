from django.http import Http404
from rest_framework import status
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView
from rest_framework import permissions

from Gedimagination.serializers import ParticipationSerializer
from Gedimagination.models import Participation


class ParticipationsList(APIView):
    permission_classes = [permissions.IsAdminUser]

    def get(self, request, format=None):
        participation = Participation.objects.all()
        serializer = ParticipationSerializer(participation, many=True)
        return Response(serializer.data)


class ParticipationsDetails(APIView):
    permission_classes = [permissions.IsAdminUser]

    def get_object(self, pk):
        try:
            return Participation.objects.get(pk=pk)
        except Participation.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serializer = ParticipationSerializer(data)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        participation = self.get_object(pk)
        serializer = ParticipationSerializer(participation, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, pk, format=None):
        participation = self.get_object(pk)
        serializer = ParticipationSerializer(participation, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

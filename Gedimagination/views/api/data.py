from django.http import Http404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import permissions

from Gedimagination.serializers import DataSerializer
from Gedimagination.models import Data


class DataApiList(APIView):

    def get(self, request, format=None):
        data = Data.objects.all()
        serializer = DataSerializer(data, many=True)
        return Response(serializer.data)


class DataApiDetails(APIView):

    def get_object(self, pk):
        try:
            return Data.objects.get(pk=pk)
        except Data.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serializer = DataSerializer(data)
        return Response(serializer.data)

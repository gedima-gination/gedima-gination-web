import datetime
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from datetime import date

from Gedimagination.lib.fileControl import *
from Gedimagination.lib.nameGenerator import *
from Gedimagination.forms.fileForm import FileForm
from Gedimagination.models import Participation, AuthUser, Data, AuthUserGroups, AuthGroup, Consentement, Action

"""
Gestion de la page participation
"""
@login_required()
def participation(request):
    #Variables
    user = request.user
    date_now = date.today().strftime("%Y-%m-%d")
    info = Data.objects.all().first()
    data = True
    file = True
    value_error = ""

    #Vérification que l'utilisateur n'a pas déjà participé
    if Participation.objects.all().filter(id_user=user.id).count() >= 1:
        response = "alreadyExist"
        return render(request, 'participation.html', {"response": response})

    #Vérification de présence d'information sur le concours
    if info is None:
        return render(request, 'error/dataError.html')
    if info.debut_participation is None or info.fin_participation is None:
        return render(request, 'error/dataError.html')

    date_debut_participation = info.debut_participation.strftime("%Y-%m-%d")
    date_fin_participation = info.fin_participation.strftime("%Y-%m-%d")

    # Vérifie la présence de données dans le formulaire
    if request.method == 'POST':
        file_form = FileForm(request.POST, request.FILES)

        if file_form.is_valid():
            extension_status = extension_control(request)
            virus_status = virus_control(request)

            extension = request.FILES['photoRealisation'].name.split('.')[-1] # Récupere l'extension du fichier
            name = generate_name(64) + "." + extension # Génère un nom de fichier
            count = Participation.objects.values_list('image', flat=True).filter(image=name).count() # Vérifie si le nom existe déjà en base de données

            # Tant que le nom généré existe déjà en base de données on génère un nouveau nom
            while count != 0:
                name = generate_name(64) + "." + extension
                count = Participation.objects.values_list('image', flat=True).filter(image=name).count()

            if extension_status and virus_status and file:
                try:
                    save_file('Gedimagination/static/images/participation/', request.FILES['photoRealisation'], name) # Sauvegarde du fichier
                    Participation(id_user=AuthUser.objects.get(id=user.id), image=name, titre=request.POST['titreRealisation'],
                                  description=request.POST['descriptionRealisation'], date_creation=request.POST['dateRealisation'], vote=0).save() # Sauvegarde de l'image dans la base de données
                    AuthUserGroups(user_id=user.id, group_id=AuthGroup.objects.filter(name='participant').first().id).save()
                    auth = AuthUser.objects.filter(id=user.id).first()
                    action = Action.objects.filter(id_action=2).first()
                    Consentement(id_user=auth, id_action=action, date_consentement=datetime.datetime.now()).save()
                    response = "success"
                except ValueError:
                    AuthUserGroups.objects.filter(user=user.id, group_id=AuthGroup.objects.filter(name='participant').first().id).delete()
                    value_error = ValueError
                    response = "error"
                    data = False
                    delete_file('Gedimagination/static/images/participation/', name)
            else:
                response = "error"

            # Retourne les différents status afin d'afficher les messages d'erreur en conséquence
            return render(request, 'participation.html', {'date_now': date_now, 'debut_participation': date_debut_participation, 'fin_participation': date_fin_participation,
                                                          'response': response, 'extension': extension_status, 'virus': virus_status,
                                                          'name': name, 'data': data, 'file': file, 'value_error': value_error})
    else:
        file_form = FileForm()

    return render(request, 'participation.html', {'date_now': date_now, 'fileForm': file_form, 'debut_participation': date_debut_participation, 'fin_participation': date_fin_participation})






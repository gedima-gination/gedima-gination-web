from rest_framework import serializers
from Gedimagination.models import Participation, Data

"""
Serializer de la table data permetant de choisir les donnée exposer
"""
class DataSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Data
        fields = ['id_data', 'debut_participation', 'fin_participation', 'debut_vote', 'fin_vote']

"""
Serializer de la table participations permetant de choisir les données exposer
"""
class ParticipationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Participation
        fields = ['id_participation', 'titre', 'description', 'date_creation', 'vote']

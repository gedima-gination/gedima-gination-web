from rest_framework import viewsets
from rest_framework import permissions
from Gedimagination.serializers import *
from Gedimagination.models import Participation, Data

"""
Gestion API pour la table DATA
"""
class DataViewSet(viewsets.ModelViewSet):
    queryset = Data.objects.filter(id_data=1)
    serializer_class = DataSerializer
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['get', 'options']
    paginator = None

"""
Gestion API pour la table Participation
"""
class ParticipationViewSet(viewsets.ModelViewSet):
    queryset = Participation.objects.all()
    serializer_class = ParticipationSerializer
    permission_classes = [permissions.IsAuthenticated]
    http_method_names = ['get', 'patch', 'put', 'options']
    paginator = None

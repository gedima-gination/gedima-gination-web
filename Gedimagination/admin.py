from django.contrib import admin
from Gedimagination.models import Data, Participation, Consentement, Action, Activation

admin.site.register(Action)
admin.site.register(Activation)
admin.site.register(Consentement)
admin.site.register(Data)
admin.site.register(Participation)


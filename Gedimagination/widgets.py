from django.forms.widgets import TextInput

"""
Widget pour avoir le calendrier pour selectionner la date
"""
class DateInput(TextInput):
    input_type = 'date'
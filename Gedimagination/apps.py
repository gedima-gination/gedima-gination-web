from django.apps import AppConfig


class GedimaginationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Gedimagination'

import vt
import environ

"""
Méthodes utiliser pour communiquer avec l'api de virus total marche partielement
"""
def scan_file(file_path):
    env = environ.Env()
    environ.Env.read_env()
    client = vt.Client(env("VIRUSTOTAL_API_KEY"))
    with open(file_path, "rb") as f:
        analysis = client.scan_file(f)

    while True:
        analysis = client.get_object("/analyses/{}", analysis.id)
        print(analysis.status)
        if analysis.status == "completed":
            return True

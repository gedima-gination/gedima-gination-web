from Gedimagination.models import Data, Participation


"""
méthodes utiliser pour générer le classement du concours
"""
def generer_classement():
    #Variables
    notes = []
    participations = Participation.objects.all().order_by('vote').reverse()
    #Boucle pour ajouter toute les notes différente dans la liste notes par exemple [8,25,20]
    for i in range(len(participations)):
        if participations[i].vote not in notes:
            notes.append(participations[i].vote)

    #Boucle pour récupérer le classement du vote selon l'index du nombre de votes par rapport a la liste notes
    for i in range(len(participations)):
        p = participations[i]
        p.place_classement = notes.index(participations[i].vote)+1
        p.save()

    #Modification des valeurs de la table data pour marquer le classement comme générer
    data = Data.objects.all().first()
    data.classement_generer = 1
    data.save()

"""
méthodes utiliser pour passer le classement publique
"""
def make_classement_publique():
    data = Data.objects.all().first()
    data.classement_publique = 1
    data.save()

"""

"""
def get_classement(participations):
    classement = [[], [], []]
    for i in range(len(participations)):
        user = participations[i].id_user
        temp = [participations[i].place_classement, participations[i].titre, participations[i].description,
                participations[i].image, participations[i].vote, user.first_name + " " + user.last_name]
        classement[participations[i].place_classement - 1].append(temp)

    return classement

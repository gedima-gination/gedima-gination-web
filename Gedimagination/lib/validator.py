from django.core.exceptions import ValidationError

"""
Validator pour le formulaire de participation pour empecher l'envoi de fichier supérieur a 5mo
"""
def file_size(value):
    limit = 5000000
    if value.size > limit:
        raise ValidationError('Le fichier envoyé est trop lourd la taille maximum est de 5mo')

"""
Validator pour le formulaire de participation pour empercher l'envoi de fichier autre que des images
"""
def file_extension(value):
    mime = value.content_type
    if mime not in ['image/jpeg', 'image/png']:
        raise ValidationError('Le fichier envoyé n\'est pas une image seul les fichier png, jpg et jpeg sont autorisé')

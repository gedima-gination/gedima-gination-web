import random
import string

"""
méthodes utiliser pour générer une chaine de charactère aléatoire d'une taille passé en parametre
"""
def generate_name(size):
    name = ""
    for i in range(size):
        name += random.choice(string.ascii_lowercase)
    return name

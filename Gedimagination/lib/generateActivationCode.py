import random
import string

"""
méthodes utiliser pour générer une chaine de charactère aléatoire d'une taille passé en parametre
"""
def generate_code():
    code = ""
    for i in range(8):
       code += random.choice(string.digits)
    return code
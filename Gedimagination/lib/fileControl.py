from django.core.files.storage import default_storage

valid_mime = ['image/jpeg', 'image/png']

"""
Méthods qui controle la taille d'un fichier passer en parametre par l'intermédiaire d'une requete
return true si le fichier est inférieur a 5mo sinon false
"""
def size_control(request):
    file = request.FILES
    size = file['photoRealisation'].size
    if size < 5000000:  # 5Mo = 5000000 octets
        return True
    else:
        return False

"""
Méthodes qui controle l'extension d'un fichier passer en parametre par l'intermédiaire d'une requete
Return true si l'extension est dans la liste valid_mime sinon return false
"""
def extension_control(request):
    file = request.FILES
    mime = file['photoRealisation'].content_type

    if mime in valid_mime:
        extension = file['photoRealisation'].name.split('.')[-1]
        if extension == "jpg" or extension == "png" or extension == "jpeg":
            return True
        else:
            return False

"""
Méthodes non implémenter permetant de controller la présence d'un virus dans le fichier passer en parametre par l'intermédiaire
d'une requete
"""
def virus_control(request):
    return True


"""
méthodes utiliser pour sauvegarder un fichier a un enplacement passer en parametre avec un nom aussi passer en parametre
"""
def save_file(path, file, file_name):
    fs = default_storage.save(path + file_name, file)
    file = default_storage.open(fs)
    file.close()

"""
méthodes permetant de supprimer un fichier en utiliser le chemin et le nom passé en parametre
"""
def delete_file(path, file_name):
    default_storage.delete(path + file_name)

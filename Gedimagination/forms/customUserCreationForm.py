from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError


class CustomUserCreationForm(UserCreationForm):
    #Champs du formulaire
    first_name = forms.CharField(label='Prenom:', min_length=0, max_length=150)
    last_name = forms.CharField(label='Nom:', min_length=0, max_length=150)
    username = forms.CharField(label='Pseudo:', min_length=5, max_length=150)
    email = forms.EmailField(label='Email:')
    password1 = forms.CharField(label='Mot de passe:', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmation mot de passe:', widget=forms.PasswordInput)
    consentement = forms.BooleanField(label="J'accepte les conditions d'utilisation", required=True)

    #Ajout des classes pour bootstrap
    first_name.widget.attrs.update({'class': 'form-control'})
    last_name.widget.attrs.update({'class': 'form-control'})
    username.widget.attrs.update({'class': 'form-control'})
    email.widget.attrs.update({'class': 'form-control'})
    password1.widget.attrs.update({'class': 'form-control'})
    password2.widget.attrs.update({'class': 'form-control'})

    """
    Méthods verifiant la validié du pseudo
    """
    def username_clean(self):
        username = self.cleaned_data['username'].lower()
        new = User.objects.filter(username=username)
        if new.count():
            raise ValidationError("User Already Exist")
        return username

    """
    Méthods verifiant la validié des mots de passe
    """
    def clean_password2(self):
        password1 = self.cleaned_data['password1']
        password2 = self.cleaned_data['password2']

        if password1 and password2 and password1 != password2:
            raise ValidationError("Password don't match")
        return password2

    """
    Méthods verifiant la validié de l'email
    """
    def email_clean(self):
        email = self.cleaned_data['email'].lower()
        new = User.objects.filter(email=email)
        if new.count():
            raise ValidationError(" Email Already Exist")
        return email

    """
    Methods qui enregistre l'inscription
    """
    def save(self, commit=True):
        user = User.objects.create_user(
            self.cleaned_data['username'],
            self.cleaned_data['email'],
            self.cleaned_data['password1'],
            first_name=self.cleaned_data['first_name'],
            last_name=self.cleaned_data['last_name']
        )

        return user

from django import forms
from Gedimagination.widgets import *

from Gedimagination.lib.validator import *


class FileForm(forms.Form):
    # Champs du formulaire
    titreRealisation = forms.CharField(label="Titre de votre projet: ", required=True, max_length=254)
    descriptionRealisation = forms.CharField(label="Description de votre projet: ", required=True, max_length=254)
    dateRealisation = forms.DateField(label="Date de votre projet: ", required=True, widget=DateInput)
    photoRealisation = forms.FileField(label="Sélectionnez l'image de votre projet: ",  validators=[file_size, file_extension], max_length=254, required=True)
    consentement = forms.BooleanField(label="J'accepte que mes données soient utilisées pour la création de votre projet: ", required=True)

    # Ajout des classes pour bootstrap
    titreRealisation.widget.attrs.update({'class': 'form-control'})
    descriptionRealisation.widget.attrs.update({'class': 'form-control'})
    dateRealisation.widget.attrs.update({'class': 'form-control'})
    photoRealisation.widget.attrs.update({'class': 'form-control'})


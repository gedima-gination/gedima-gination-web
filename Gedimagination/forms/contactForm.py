from django import forms


class ContactForm(forms.Form):
    # Champs du formulaire
    full_name = forms.CharField(label='Nom et Prénom', required=True)
    email = forms.EmailField(label='Email:', required=True)
    message = forms.CharField(label="Reglement", required=True,widget=forms.Textarea)

    # Ajout des classes pour bootstrap
    full_name.widget.attrs.update({'class': 'form-control'})
    email.widget.attrs.update({'class': 'form-control'})
    message.widget.attrs.update({'class': 'form-control', 'style': 'height: 100px;'})
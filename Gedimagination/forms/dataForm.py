from django import forms

from Gedimagination.widgets import DateInput


class DataForm(forms.Form):
    # Champs du formulaire
    debut_participation = forms.DateField(label="Debut participation: ", required=True, widget=DateInput)
    fin_participation = forms.DateField(label="Fin participation: ", required=True, widget=DateInput)
    debut_vote = forms.DateField(label="Debut vote: ", required=True, widget=DateInput)
    fin_vote = forms.DateField(label="Fin vote: ", required=True, widget=DateInput)
    reglement = forms.CharField(label="Reglement", required=True,widget=forms.Textarea)

    # Ajout des classes pour bootstrap
    debut_participation.widget.attrs.update({'class': 'form-control'})
    fin_participation.widget.attrs.update({'class': 'form-control'})
    debut_vote.widget.attrs.update({'class': 'form-control'})
    fin_vote.widget.attrs.update({'class': 'form-control'})
    reglement.widget.attrs.update({'class': 'form-control', 'style': 'height: 100px;'})



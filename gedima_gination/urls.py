from django.contrib import admin
from django.urls import path, include, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view

from Gedimagination.views.api.TOTPAPI import TOTPCreateView, TOTPVerifyView
from Gedimagination.views.api.customAuthToken import CustomAuthToken
from Gedimagination.views.index import index
from Gedimagination.views.contact import contact
from Gedimagination.views.registration.activate import activate
from Gedimagination.views.reglement import reglement
from Gedimagination.views.classement import classement
from Gedimagination.views.participation import *
from Gedimagination.views.registration.register import *
from Gedimagination.views.api.data import *
from Gedimagination.views.api.participations import *
from Gedimagination.views.api.apiRoot import *
from Gedimagination.views.user import user
from Gedimagination.views.admin import admin as manager

#Activation de la double authtification sur la page admin
#admin.site.__class__ = OTPAdminSite

schema_view = get_schema_view(
   openapi.Info(
      title="Gedimagination api",
      default_version='v1.0',
      description="API pour le concours Gedimagination",
      terms_of_service="",
      contact=openapi.Contact(email=""),
      license=openapi.License(name=""),
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)


urlpatterns = [
    #Gestion des comptes

    path('admin/', admin.site.urls, name='admin'),
    path("accounts/", include("django.contrib.auth.urls")),
    #path('account/', include('allauth.urls')), #Gestionnaire de connexion avec un compte google
    path('accounts/register/', register, name='register'),
    path('accounts/manager/', manager, name='manager'),
    path('accounts/user/', user, name='user'),
    path('activate/<slug:uidb64>/<slug:token>/', activate, name='activate'),

    #Sites
    path('', index, name='index'),
    path('participation/', participation, name='participation'),
    path('classement/', classement, name='classement'),
    path('reglement/', reglement, name='reglement'),
    path('contact/', contact, name='contact'),

    #API
    re_path(r'^api/swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^api/swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^api/redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    re_path(r'^totp/create/$', TOTPCreateView.as_view(), name='totp-create'),
    re_path(r'^totp/login/(?P<token>[0-9]{6})/$', TOTPVerifyView.as_view(), name='totp-login'),
    path('api/participations/', ParticipationsList.as_view()),
    path('api/participations/<int:pk>/', ParticipationsDetails.as_view()),
    path('api/data/', DataApiList.as_view()),
    path('api/data/<int:pk>/', DataApiDetails.as_view()),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api-token-auth/', CustomAuthToken.as_view(), name='api-token-auth'),
]

#Gestion des erreurs
handler418 = 'Gedimagination.views.handler.handler418'
handler400 = 'Gedimagination.views.handler.handler400'
handler401 = 'Gedimagination.views.handler.handler401'
handler403 = 'Gedimagination.views.handler.handler403'
handler404 = 'Gedimagination.views.handler.handler404'
handler500 = 'Gedimagination.views.handler.handler500'
